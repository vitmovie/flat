# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 16:34:17 2017

@author: пользователь
"""

import unittest

def isint(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

NotCorrectPin = 'Is Not Correct PIN'

class Card:
    def __init__(self, id_card, pin, money, name, status):
        self.id_card = id_card
        if isint(pin) and 1000<=pin and pin<=9999:
            self.pin = pin
        else:
            raise ValueError(NotCorrectPin)
        self.money = money
        self.name = name
        self.status = status
    
    def GetPin(self):
        return self.pin
    
    def GetMoney(self):
        return self.money
    
    def GetStatus(self):
        return self.status

    def GetIdCard(self):
        return self.id_card
    
    def GetName(self):
        return self.name
    
    def SetIdCard(self, id_card):
        self.id_card = id_card
      
    def SetPin(self, pin):
        self.pin = pin
        
    def SetMoney(self, money):
        self.money = money
        
    def SetName(self, name):
        self.name = name
        
    def SetStatus(self, status):
        self.status = status
    
    def Checking(self, pin, status):
        if self.GetPin() == pin and self.GetStatus() == 1 :
            print('checking is success')
        else:
            if not(self.GetPin() == pin): 
                print('pin is not correct')
            else:
                print('your card is blocked')
    
    def ToCash(self, money, trueToCash):
        if trueToCash == 1 and self.money >= money:
            self.money = self.GetMoney() - money
        else:
            print('do not cash: ', money)
    
class Bankomat:
    def __init__(self, money, limit):
        self.money = money 
        self.limit = limit
        
    def GetMoney(self):
        return self.money
    
    def SetMoney(self, money):
        self.money = money
    
    def GetLimit(self):
        return self.limit
    
    def SetLimit(self, limit):
        self.limit = limit
    
    def ToCash(self, money):
        if money <= self.GetLimit():
            self.money = self.GetMoney() - money
        else:
            print('You want to get money biggest than maybe to cash (', money, ')' )
    
                                  
        
class SetTest(unittest.TestCase):
    
    def test_PinIsNotCorrect(self):
        try:
            self.card = Card(1111, 'asdf', 10000, 'empty', 1)
        except ValueError:
            self.assertEqual(NotCorrectPin, 'Is Not Correct PIN' )
    
    def test_PinIsCorrect(self):
        self.card = Card(1111, 1234, 10000, 'empty', 1)
        self.assertEqual(self.card.GetPin(), 1234)
        
    
    def test_StatusIsTrue(self):
        self.card = Card(1111, 1234, 10000, 'empty', 1)
        self.assertEqual(self.card.GetStatus(), 1)
        
    def test_StatusIsBlock(self):
        self.card = Card(1111, 1234, 0, 'empty', 0)
        self.assertEqual(self.card.GetStatus(), 0)
    
    def test_RightToCash(self):
        self.card = Card(1111, 1234, 10000, 'empty', 1)
        self.bankomat = Bankomat(100000, 60000)
        self.bankomat.ToCash(5000)
        self.card.ToCash(5000, 1)
        self.assertEqual(self.card.GetMoney(), 5000)
        self.assertEqual(self.bankomat.GetMoney(), 95000)
    
    def test_CardToCashBiggestBankomat(self):
        self.card = Card(1111, 1234, 100000, 'empty', 1)
        self.bankomat = Bankomat(50000, 60000)
        self.card.ToCash(70000, 0)
        self.bankomat.ToCash(70000)
        self.assertEqual(self.card.GetMoney(), 100000)
        self.assertEqual(self.bankomat.GetMoney(), 50000)
        
    def test_ToCashBiggestThanCardHas(self):
        self.card = Card(1111, 1234, 10000, 'empty', 1)
        self.bankomat = Bankomat(100000, 60000)
        self.card.ToCash(15000, 1)
        #self.bankomat.ToCash(15000)
        self.assertEqual(self.card.GetMoney(), 10000)
        self.assertEqual(self.bankomat.GetMoney(), 100000)
        
    
    #def test_
    
    
        
        
    
                                 
                                  
if __name__ == '__main__':
    unittest.main()   
    #card = Card(1234, '1234sd', 1234, 1234, 1234)
    
#print(card.GetMoney())  
#card.ToCash(1000)
#print(card.GetMoney())    

    
    
    
    