import cv2
# from PIL import Image
import numpy as np
from matplotlib import pyplot as plt
import time
from scipy import signal
from math import sqrt
from math import pi
from math import cos, sin
from PIL import Image
import PIL
import random

sobX = np.float32([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])
sobY = sobX.T


def computeSobelNpDx(img):
    n = img.shape[0]
    m = img.shape[1]
    gradientX = np.zeros((n, m))
    nulNul = img[0:n - 2, 0:m - 2] * sobX[0, 0]
    nulFirst = img[0:n - 2, 1:m - 1] * sobX[0, 1]
    nulSecond = img[0:n - 2, 2:m] * sobX[0, 2]
    secNul = img[2:n, 0:m - 2] * sobX[2, 0]
    secFirst = img[2:n, 1:m - 1] * sobX[2, 1]
    secSecond = img[2:n, 2:m] * sobX[2, 2]
    gradientX = nulNul + nulFirst + nulSecond + secNul + secSecond + secFirst

    return gradientX


def computeSobelNpDy(img):
    n = img.shape[0]
    m = img.shape[1]
    gradientY = np.zeros((n, m))
    nulNul = img[0:n - 2, 0:m - 2] * sobY[0, 0]
    firstNul = img[1:n - 1, 0:m - 2] * sobY[1, 0]
    secondNul = img[2:n, 0:m - 2] * sobY[2, 0]
    nulSecond = img[0:n - 2, 2:m] * sobY[0, 2]
    firstSecond = img[1:n - 1, 2:m] * sobY[1, 2]
    secSecond = img[2:n, 2:m] * sobY[2, 2]
    gradientY = nulNul + firstNul + secondNul + nulSecond + firstSecond + secSecond
    return gradientY


def non_maximum_supression(matr, n, threshold):
    delta1 = matr.shape[0] / (n + 1)
    delta2 = matr.shape[1] / (n + 1)
    maxes = []
    for i in range(1, int(delta1) + 1, 1):  # int(delta1)+1
        for j in range(1, int(delta2) + 1, 1):
            m1, m2 = maxInBlock(i * (n + 1), j * (n + 1), n, matr)  # нашли макс в блоке размерности (n+1)(n+1)
            if matr[m1, m2] > threshold:
                if (existIn2n_1Block(m1, m2, i * (n + 1), j * (n + 1), matr.shape[0], matr.shape[1], matr,
                                     n)):  # если есть элемент который
                    # больше внутри квадрат (2n+1)x(2n+1) то этот максмум не записываем в список
                    # print(matr[m1,m2])
                    maxes.append((m1, m2, matr[m1, m2]))
    return np.asarray(maxes)


def existIn2n_1Block(m1, m2, h, w, main_h, main_w, matr,
                     n):  # m1, m2 - координаты максимум, h , w - координаты правого нижнего угла блока n+1,
    # main_h, main_w - размеры всей матрицы

    # проверяем выходы за пределы рамзерности матрицы
    s1 = m1 - n  # индекс строки начала блока (2n+1)(2n+1)
    while (s1 < 0):
        s1 = s1 + 1  # сдвигаем верхнюю границу блока 2n+1 на 1 пиксель вниз (размерность этого блока будет меньше соответсвенно)
    s2 = m2 - n  #
    # s1, s2 - коориднаты левого верхнего угла блока (2n+1)(2n+1)
    while s2 < 0:
        s2 = s2 + 1
    k1 = m1 + n
    while k1 > main_h - 1:
        k1 = k1 - 1
    k2 = m2 + n
    while k2 > main_w - 1:
        k2 = k2 - 1
        # (s1, s2) - коориднаты левого верхнего угла блока (2n+1)(2n+1)
        # (k1, k2) - координаты правого нижнего
        # (k1,s2) - координаты нижнего левого угла
        # (s1, k2) - координаты правого верхнего

    # разбиваем на подблоки окна раземар 2n+1 на 2n+1 чтобы не сравнивать элементы которые мы уже сравнивали
    sub_right = matr[s1:k1 + 1, w:k2 + 1]
    sub_left = matr[s1:k1 + 1, s2:w - (n + 1)]
    sub_bottom = matr[h:k1 + 1, w - (n + 1):w]
    sub_top = matr[s1:h - (n + 1), w - (n + 1):w]

    if not not sub_right.any():  # просто is не работало и не хотелось писать else поэтому not not
        if sub_right.max() > matr[m1, m2]:
            return False
    if not not sub_left.any():
        if sub_left.max() > matr[m1, m2]:
            return False
    if not not sub_bottom.any():
        if sub_bottom.max() > matr[m1, m2]:
            return False
    if not not sub_top.any():
        if sub_top.max() > matr[m1, m2]:
            return False

    return True


def maxInBlock(h, w, n, matr):  # ищем максимум в блоке размера n+1
    max = 0
    max_i, max_j = h - (n + 1), w - (n + 1)
    for i in range(h - (n + 1), h, 1):
        for j in range(w - (n + 1), w, 1):
            if (matr[i, j] > max):
                max = matr[i, j]
                max_i = i
                max_j = j

    return max_i, max_j


def circleOne(flagOptimization):
    image = Image.open('rsz_3.jpg').convert('L')  # L - режим в серо изображение 1 канал

    img = np.float32(image)
    sobX = np.float32([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])
    sobY = sobX.T
    gradientX = computeSobelNpDx(img)
    gradientY = computeSobelNpDy(img)
    G3 = np.sqrt(gradientX * gradientX + gradientY * gradientY)
    T = np.mean(G3)
    theta = 2 * pi
    ind2 = (G3 <= T)
    ind = (G3 > T)
    step = theta / 360
    G3[ind] = 0
    G3[ind2] = 1
    # G3 = np.sqrt(dx * dx + dy * dy)
    a = G3.shape[0]
    b = G3.shape[1]
    maxR = min(img.shape[0], img.shape[1])
    maxR = int(np.sqrt(maxR ** 2 + maxR ** 2) / 2)
    cumCircle = np.zeros((a, b, maxR))
    if (flagOptimization):
        indexes = np.zeros((a * a + b * b, 2))
        countZerosInG = 0
        i = 0
        j = 0
        for x in range(0, a, 1):
            for y in range(0, b, 1):
                if G3[x, y] == 0:
                    countZerosInG += 1
                    indexes[i, j] = x
                    indexes[i, j + 1] = y
                    i += 1
        twentyProcentCountZerosInG = int(countZerosInG / 5)
        for i in range(0, twentyProcentCountZerosInG, 1):
            t = random.randint(0, countZerosInG)
            x = indexes[t, 0]
            y = indexes[t, 1]
            for i in range(0, a, 1):
                for j in range(0, b, 1):
                    r = int(np.sqrt((i - x) ** 2 + (j - y) ** 2))
                    if r > 0 and r < maxR:
                        cumCircle[i, j, r] += 1
                        print(r)

    else:
        for x in range(0, a, 1):
            for y in range(0, b, 1):
                if G3[x, y] == 0:
                    for i in range(0, a, 1):
                        for j in range(0, b, 1):
                            r = int(np.sqrt((i - x) ** 2 + (j - y) ** 2))
                            if r > 0 and r < maxR:
                                cumCircle[i, j, r] += 1
                                print(r)

    cumCircleNms = []
    for i in range(0, maxR, 1):
        flag = cumCircle[0:a, 0:b, i]
        print(flag)
        print(flag.shape)
        if (flagOptimization):
            #CumCirMNS = non_maximum_supression(flag, 50, 62)
            CumCirMNS = non_maximum_supression(flag, 50, 62)
        else:
            CumCirMNS = non_maximum_supression(flag, 30, 150)

        print(CumCirMNS)
        if not not CumCirMNS.any():
            temp = np.mat(CumCirMNS)
            # temp = CumCirMNS[0:len(CumCirMNS),0:2]
            cumCircleNms.append((temp, i))  # получили список всех максов a,b для всех r

    myCircle = cv2.imread('rsz_3.jpg')
    print(cumCircleNms)
    for coords, r in cumCircleNms:
        # print(coords, r)

        for i in range(0, len(coords)):
            # print(coords[i, 0])
            # print(coords[i, 1])
            # print(coords[i, 2])

            cv2.circle(myCircle, (int(coords[i, 0]), int(coords[i, 1])), r, (0, 255, 0))

    cv2.imwrite('titleCircle.png', myCircle)


def lineOne(flagOptimization):
    image = Image.open('rsz_plitka.jpg').convert('L')  # L - режим в серо изображение 1 канал
    img = np.float32(image)
    sobX = np.float32([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])
    sobY = sobX.T
    gradientX = computeSobelNpDx(img)
    gradientY = computeSobelNpDy(img)
    G3 = np.sqrt(gradientX * gradientX + gradientY * gradientY)
    T = np.mean(G3)
    theta = 2 * pi
    ind2 = (G3 <= T)
    ind = (G3 > T)
    step = theta / 360
    G3[ind] = 0
    G3[ind2] = 1
    # G3 = np.sqrt(dx * dx + dy * dy)
    a = G3.shape[0]
    b = G3.shape[1]
    roMax = sqrt(a * a + b * b)
    ro = np.array
    cum = np.zeros((int(roMax), 360))
    if (flagOptimization):
        indexes = np.zeros((a * a + b * b, 2))
        i = 0
        j = 0
        countZerosInG = 0
        cum = np.zeros((int(roMax), 360))
        for x in range(0, a, 1):
            for y in range(0, b, 1):
                if G3[x, y] == 0:
                    indexes[i, j] = x
                    indexes[i, j + 1] = y
                    i += 1
                    countZerosInG += 1
        twentyProcentCountZerosInG = int(countZerosInG / 5)
        for i in range(0, twentyProcentCountZerosInG, 1):
            t = random.randint(0, countZerosInG)
            x = indexes[t, 0]
            y = indexes[t, 1]
            for j in range(0, 360, 1):
                roI = int(x * cos(j) + y * sin(j))
                accurasy = x * cos(j) + y * sin(j) - roI
                if roI > 0 and roI < int(roMax) and abs(accurasy) < 0.35:
                    cum[roI, j] += 1
    else:
        for x in range(0, a, 1):
            for y in range(0, b, 1):
                if G3[x, y] == 0:
                    for j in range(0, 360, 1):
                        roI = int(x * cos(j) + y * sin(j))
                        accurasy = x * cos(j) + y * sin(j) - roI
                        if roI > 0 and roI < int(roMax) and abs(accurasy) < 0.35:
                            cum[roI, j] += 1
    if (flagOptimization):
        cumnms = non_maximum_supression(cum, 42, 15)
    else:
        cumnms = non_maximum_supression(cum, 60, 0)
    print(cumnms)
    print(len(cumnms))
    CMNS = np.mat(cumnms)
    CMNS = CMNS[0:len(cumnms), 0:2]
    myIm = cv2.imread('rsz_plitka.jpg')
    Points = np.zeros((img.shape[1], 2))
    for i in range(0, CMNS.shape[0], 1):
        x1 = -1000
        x2 = img.shape[0]+1000
        if not (CMNS[i, 1] == 0 or CMNS[i, 1] == 180):
            # y1 = int((CMNS[i, 0] - x1 * cos(pi-CMNS[i, 1])) / sin(pi-CMNS[i, 1]))
            # y2 = int((CMNS[i, 0] - x2 * cos(pi-CMNS[i, 1])) / sin(pi-CMNS[i, 1]))
            y1 = int((CMNS[i, 0] - x1 * cos(CMNS[i, 1])) / sin(CMNS[i, 1]))
            y2 = int((CMNS[i, 0] - x2 * cos(CMNS[i, 1])) / sin(CMNS[i, 1]))
            # if y1 >= 0 and y1 <= img.shape[1] and y2 >= 0 and y2 <= img.shape[1]:
            cv2.line(myIm, (y1, x1), (y2, x2), (0, 255, 0), 2)
        # y1 = 0
        # y2 = img.shape[1]
        # if not (CMNS[i, 1] == 90 or CMNS[i, 1] == 270):
        #     x1 = int((CMNS[i, 0] - y1 * sin(CMNS[i, 1])) / cos(CMNS[i, 1]))
        #     x2 = int((CMNS[i, 0] - y2 * sin(CMNS[i, 1])) / cos(CMNS[i, 1]))
        #     if x1 >= 0 and x1 <= img.shape[0] and x2 >= 0 and x2 <= img.shape[0]:
        #         cv2.line(myIm, (y1, x1), (y2, x2), (0, 255, 0), 2)

    cv2.imwrite('titleLine.png', myIm)


lineOne(1)
circleOne(1)