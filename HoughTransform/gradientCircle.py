import cv2
# from PIL import Image
import numpy as np
from matplotlib import pyplot as plt
import time
from scipy import signal
from math import sqrt, atan
from math import pi
from math import cos, sin, atan2
from PIL import Image
import PIL
import random



def non_maximum_supression(matr, n, threshold):
    delta1 = matr.shape[0] / (n + 1)
    delta2 = matr.shape[1] / (n + 1)
    maxes = []
    for i in range(1, int(delta1) + 1, 1):  # int(delta1)+1
        for j in range(1, int(delta2) + 1, 1):
            m1, m2 = maxInBlock(i * (n + 1), j * (n + 1), n, matr)  # нашли макс в блоке размерности (n+1)(n+1)
            if matr[m1, m2] > threshold :
                if (existIn2n_1Block(m1, m2, i * (n + 1), j * (n + 1), matr.shape[0], matr.shape[1], matr,
                                     n)):  # если есть элемент который
                    # больше внутри квадрат (2n+1)x(2n+1) то этот максмум не записываем в список
                    # print(matr[m1,m2])
                    maxes.append((m1, m2, matr[m1, m2]))
    return np.asarray(maxes)


def existIn2n_1Block(m1, m2, h, w, main_h, main_w, matr,
                     n):  # m1, m2 - координаты максимум, h , w - координаты правого нижнего угла блока n+1,
    # main_h, main_w - размеры всей матрицы

    # проверяем выходы за пределы рамзерности матрицы
    s1 = m1 - n  # индекс строки начала блока (2n+1)(2n+1)
    while (s1 < 0):
        s1 = s1 + 1  # сдвигаем верхнюю границу блока 2n+1 на 1 пиксель вниз (размерность этого блока будет меньше соответсвенно)
    s2 = m2 - n  #
    # s1, s2 - коориднаты левого верхнего угла блока (2n+1)(2n+1)
    while s2 < 0:
        s2 = s2 + 1
    k1 = m1 + n
    while k1 > main_h - 1:
        k1 = k1 - 1
    k2 = m2 + n
    while k2 > main_w - 1:
        k2 = k2 - 1
        # (s1, s2) - коориднаты левого верхнего угла блока (2n+1)(2n+1)
        # (k1, k2) - координаты правого нижнего
        # (k1,s2) - координаты нижнего левого угла
        # (s1, k2) - координаты правого верхнего

    # разбиваем на подблоки окна раземар 2n+1 на 2n+1 чтобы не сравнивать элементы которые мы уже сравнивали
    sub_right = matr[s1:k1 + 1, w:k2 + 1]
    sub_left = matr[s1:k1 + 1, s2:w - (n + 1)]
    sub_bottom = matr[h:k1 + 1, w - (n + 1):w]
    sub_top = matr[s1:h - (n + 1), w - (n + 1):w]

    if not not sub_right.any():  # просто is не работало и не хотелось писать else поэтому not not
        if sub_right.max() > matr[m1, m2]:
            return False
    if not not sub_left.any():
        if sub_left.max() > matr[m1, m2]:
            return False
    if not not sub_bottom.any():
        if sub_bottom.max() > matr[m1, m2]:
            return False
    if not not sub_top.any():
        if sub_top.max() > matr[m1, m2]:
            return False

    return True


def maxInBlock(h, w, n, matr):  # ищем максимум в блоке размера n+1
    max = 0
    max_i, max_j = h - (n + 1), w - (n + 1)
    for i in range(h - (n + 1), h, 1):
        for j in range(w - (n + 1), w, 1):
            if (matr[i, j] > max):
                max = matr[i, j]
                max_i = i
                max_j = j

    return max_i, max_j


# image = Image.open('rsz_3.jpg').convert('L')  # L - режим в серо изображение 1 канал
image = Image.open('rsz_circles.png').convert('L')  # L - режим в серо изображение 1 канал
img = np.float32(image)
sobX = np.float32([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])
sobY = sobX.T


dx = signal.convolve(img, sobX, mode='same')
dy = signal.convolve(img, sobY, mode='same')
G3 = np.sqrt(dx*dx + dy*dy)
Sobel = G3
# cv2.imwrite('lol.png', G3)

T = np.mean(G3)
theta = 2 * pi
ind2 = (G3 <= T)
ind = (G3 > T)
step = theta / 360

G3[ind] = 0
G3[ind2] = 1

a = G3.shape[0]
b = G3.shape[1]

# original_image = cv2.imread('rsz_3.jpg', 1)
original_image = cv2.imread('rsz_circles.png', 1)
blur_image = cv2.GaussianBlur(original_image, (3, 3), 0)

edged_image = cv2.Canny(blur_image, 75, 150)
ind = (edged_image == 255)
edges = np.where(edged_image == 255)


maxR = min(img.shape[0], img.shape[1])
maxR = int(np.sqrt(maxR ** 2 + maxR ** 2) / 2)
cum = np.zeros((a, b,maxR))
# случай для градиента
for l in range (0,1000):
    i = random.randint(0,len(edges[0])-1)
    if dx[edges[0][i],edges[1][i]] ==0:
        continue
    theta = atan(dy[edges[0][i],edges[1][i] ]/dx[edges[0][i],edges[1][i]]) # смотрим направление градиента
    theta2 = theta+pi
    print(theta)
    y = edges[1][i]
    x = edges[0][i]
    y1 = y
    x1 = x
    ro = 1
    cum[int(x),int(y)]=-1
    for k in range (1,maxR):
        # ходим вдоль направления градиента, и там где мы больше всего проголсуем, то там будет наш центр
        # print(x,y)
        x = x + ro*cos(theta)
        y = y + ro*sin(theta)
        x1 = x1 + ro*cos(theta2)
        y1 = y1 + ro*sin(theta2)

        if not (x < 0 or y < 0 or x > a-1 or y > b-1):
            cum[int(x),int(y),k] += 1
        if not (x1 < 0 or y1 < 0 or x1 > a-1 or y1 > b-1) :
           cum[int(x1),int(y1),k]+= 1

    # cv2.line(original_image, (edges[0][i], edges[1][i]), (int(x1), int(y1)), (255, 0, 0),1) # рисуем градиенты
    #
    # cv2.line(original_image, (edges[0][i], edges[1][i]), (int(x), int(y)), (255, 0, 0),1)


cumCircleNms = []
for i in range(0,maxR,1): # бегаем вдоль куба по r
    flag = cum[0:a,0:b,i]
    CumCirMNS = non_maximum_supression(flag,40,29) # если мы меняем l то должны менять и 3 параметр
    if not not CumCirMNS.any():
        temp = np.mat(CumCirMNS)
        cumCircleNms.append((temp,i)) # получили список всех максов a,b для всех r

print(cumCircleNms)

for coords, r in cumCircleNms: # по всем максам
    for i in range (0, len(coords)):
        cv2.circle(original_image, (int(coords[i,0]), int(coords[i,1])), r, (0, 255, 0),2)

cv2.imwrite('result.png',original_image)

