from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from fltAp.serializers import FlatSerializer
from fltClnt.models import Flat


class FlatViewSet(viewsets.ModelViewSet):
    queryset = Flat.objects.all()
    serializer_class = FlatSerializer