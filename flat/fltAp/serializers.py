from django.db.migrations import serializer
from rest_framework import serializers

from fltClnt.models import Client, Flat


class ClientSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Client
        fields = ('surName', 'firstName', 'patronymic', 'phone')

class FlatSerializer(serializers.HyperlinkedModelSerializer):

    client = ClientSerializer()

    class Meta:
        model = Flat
        fields = ('number', 'price', 'plan', 'client')





