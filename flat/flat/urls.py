from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from fltAp import views

router = routers.DefaultRouter()
router.register(r'flats', views.FlatViewSet)

urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url('admin/', admin.site.urls),
    url('api/', include(router.urls)),
    url(r'^', include('fltClnt.urls')),
    #url(r'^', include(router.urls)),
]
