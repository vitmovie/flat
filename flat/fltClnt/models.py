from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.utils import timezone


class Client(models.Model):
    class Meta():
        db_table = 'client'

    surName = models.CharField(max_length=50)
    firstName = models.CharField(max_length=50)
    patronymic = models.CharField(max_length=50)
    phone = PhoneNumberField()

    def __str__(self):
        return self.surName


class Flat(models.Model):
    class Meta():
        db_table = "flat"

    number = models.CharField(max_length=20)
    price = models.DecimalField(decimal_places=2,max_digits=15)
    plan = models.TextField()
    client = models.ForeignKey(Client,
                               on_delete=models.CASCADE,)

    def __str__(self):
        return self.number