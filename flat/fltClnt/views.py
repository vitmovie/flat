from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render

# Create your views here.
from requests import Response
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse

from fltClnt.models import Flat, Client
# from fltClnt.serializers import FlatSerializer


def toMain(request):
    return render(request, 'main.html', locals())

def flatList(request):
    try:
        flats = Flat.objects.all()
    except ObjectDoesNotExist:
        print("ObjectDoesNotExist")
    return render(request, 'flats.html', locals())

def flatShow(request, number):
    try:
        flat = Flat.objects.get(number=number)
        client = Client.objects.get(id=flat.client_id)
    except ObjectDoesNotExist:
        print('ObjectDoesNotExist')
    return render(request, 'flat.html', locals())

def clientShow(request, number, id):
    try:
        flat = Flat.objects.get(number=number)
        client = Client.objects.get(id=id)
    except ObjectDoesNotExist:
        print('ObjectDoesNotExist')
    return render(request, 'client.html', locals())


