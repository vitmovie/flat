from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from fltClnt import views

urlpatterns = [
    url(r'^$', views.toMain),
    url(r'^flatList/', views.flatList),
    url(r'^(?P<number>[^/]+)/(?P<id>[^/]+)/',views.clientShow),
    url(r'^(?P<number>[^/]+)/', views.flatShow),
]
