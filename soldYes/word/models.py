from django.db import models

# Create your models here.
from django.utils import timezone


class Word(models.Model):
    class Meta():
        db_table = "word"
    word = models.CharField(max_length=200)
    url = models.URLField()
    count = models.IntegerField(default=0)
    dt = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.word
