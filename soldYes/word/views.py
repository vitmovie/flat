import collections
import re
import urllib

import lxml.html
from django.shortcuts import render
from word.forms import WordForm
from word.models import Word


def formCreator(request):
    form = WordForm()
    return render(request, 'word.html', locals())

def counter(request):
    if request.POST:
        form = WordForm(request.POST)
        if form.is_valid():
            wrd = form.save()
            w = Word.objects.filter(word=wrd.word,url=wrd.url).first()
            word = w.word
            url = w.url

            text = urllib.request.urlopen(url).read()
            doc = lxml.html.fromstring(text)
            lst = doc.xpath(r'.//text()')

            pat = re.compile(r'\b\S+\b')
            words = sum(map(pat.findall, lst), [])

            counted = collections.Counter(words)
            w.count = counted[word]

            if w.count > 0:
                wordForBD = Word(word=word,url=url,count=w.count)
                wordForBD.save()
                Word.objects.get(word=word,url=url,count=0).delete()

    return render(request, 'submited.html', locals())


