from django.forms import ModelForm

from word.models import Word


class WordForm(ModelForm):
    class Meta:
        model = Word
        fields = ['word', 'url']