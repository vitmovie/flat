from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from word import views

urlpatterns = [
    url(r'^$', views.formCreator),
    url(r'^submited/', views.counter),
]
